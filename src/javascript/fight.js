import { createElement } from "./helpers/domHelper";
import { healthIcon } from "./constants/icons";

export function fight(firstFighter, secondFighter) {
    buildFightArena(firstFighter, secondFighter);

    let firstFighterHealth = firstFighter.health;
    let secondFighterHealth = secondFighter.health;
    let winner;

    while (firstFighterHealth > 0 && secondFighterHealth > 0) {
        let damage = getDamage(firstFighter, secondFighter);
        if (damage > 0) {
            secondFighterHealth -= damage;
        }
        damage = getDamage(firstFighter, secondFighter);
        if (damage > 0) {
            firstFighterHealth -= damage;
        }

        if(firstFighterHealth <= 0) {
            winner = secondFighter;
        }else if(secondFighterHealth <= 0) {
            winner = firstFighter;
        }
    }
    return winner;
}

export function getDamage(attacker, enemy) {
    return getHitPower(attacker) - getBlockPower(enemy);
}

export function getHitPower(fighter) {
    let criticalHitChance = Math.random() + 1;
    return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
    let dodgeChance = Math.random() + 1;
    return fighter.defense * dodgeChance;
}
function buildFightArena(firstFighter, secondFighter) {
    const root = document.getElementById("root");
    document.getElementsByClassName('fighters')[0].style.display = 'none';

    const container = createElement({ tagName: 'div', className: 'container'})
    const firstFighterBlock = createElement({ tagName: 'div', className: '',
        attributes:{ style: 'display:inline-block'}});
    const firstFighterName = createElement({ tagName: 'h1', className: ''})
    firstFighterName.innerText = firstFighter.name;
    const firstFighterImg = createElement({ tagName: 'img', className: 'custom-fighter-img',
        attributes: {src: firstFighter.source}})
    const firstFighterHealth = createElement({ tagName: 'img', className: 'custom-icon-img',
            attributes: {src: healthIcon }});
    const firstFighterHealthValue = createElement({ tagName: 'span', className: 'custom-character-text'});
    firstFighterHealthValue.innerText = firstFighter.health;
    firstFighterBlock.append(firstFighterName, firstFighterImg, firstFighterHealth, firstFighterHealthValue);

    const secondFighterBlock = createElement({ tagName: 'div', className: '',
        attributes:{ style: 'display:inline-block'}});
    const secondFighterName = createElement({ tagName: 'h1', className: ''})
    secondFighterName.innerText = secondFighter.name;
    const secondFighterImg = createElement({ tagName: 'img', className: 'custom-fighter-img',
        attributes: {src: secondFighter.source}})
    const secondFighterHealth = createElement({ tagName: 'img', className: 'custom-icon-img',
        attributes: {src: healthIcon }});
    const secondFighterHealthValue = createElement({ tagName: 'span', className: 'custom-character-text'});
    secondFighterHealthValue.innerText = secondFighter.health;
    secondFighterBlock.append(secondFighterName, secondFighterImg, secondFighterHealth, secondFighterHealthValue);

    const fightImg = createElement({ tagName: 'img', className: 'custom-system-img',
        attributes: {src: 'resources/fight.png'}})

    container.append(firstFighterBlock,  secondFighterBlock);
    root.append(fightImg, container);
}