import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { healthIcon, attackIcon, defenseIcon} from "../constants/icons";

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, health, attack, defense, quote, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'h2', className: 'custom-fighter-name' });
  const imgElement = createElement({ tagName: 'img', className: 'custom-fighter-img',
                          attributes: {src: source}})

  const quoteElement = createElement({ tagName: 'p', className: 'custom-quote'});
  quoteElement.innerText = quote;


  // Character's characteristics

  const healthElement = createElement({ tagName: 'div', className: 'custom-character-block'});

  const healthImg = createElement({
        tagName: 'img', className: 'custom-icon-img',
        attributes: {src: healthIcon}
  });
  const healthValue = createElement({
        tagName: 'span', className: 'custom-character-text'
  });

  healthValue.innerText = health;
  healthElement.append(healthImg, healthValue);

  const attackElement = createElement({ tagName: 'div', className: 'custom-character-block'});

  const attackImg = createElement({
    tagName: 'img', className: 'custom-icon-img',
    attributes: {src: attackIcon}
  });
  const attackValue = createElement({
    tagName: 'span', className: 'custom-character-text'
  });
  attackValue.innerText = attack;
  attackElement.append(attackImg, attackValue);

  const defenseElement = createElement({ tagName: 'div', className: 'custom-character-block'});

  const defenseImg = createElement({
    tagName: 'img', className: 'custom-icon-img',
    attributes: {src: defenseIcon}
  });
  const defenseValue = createElement({
      tagName: 'span', className: 'custom-character-text'
  });
  defenseValue.innerText = defense;
  defenseElement.append(defenseImg, defenseValue);

  nameElement.innerText = name;
  fighterDetails.append(nameElement, imgElement, quoteElement, healthElement, attackElement, defenseElement);

  return fighterDetails;
}