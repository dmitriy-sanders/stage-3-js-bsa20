import {createElement} from "../helpers/domHelper";
import {showModal} from "./modal";

export  function showWinnerModal(fighter) {

    const title = 'Winner!';
    const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });

    const imgElement = createElement({ tagName: 'img', className: 'custom-fighter-img',
        attributes: {src: fighter.source}})

    const headerElement = createElement({ tagName: 'h2', className:'', attributes: {style: 'text-align: center'}})
    headerElement.innerText = fighter.name;

    const playAgainBtn = createElement({ tagName: 'a', className: 'custom-btn',
        attributes: { href: '/'}})
    playAgainBtn.innerText = "Play Again!";

    bodyElement.append(headerElement, imgElement, playAgainBtn);
    showModal({title, bodyElement})


}